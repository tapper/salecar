import {Http, Headers, Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import {HttpModule} from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";



const ServerUrl = "http://tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class CompanyService {

    public CompaniesArray: any[] = [];
    public CarsArray: any[] = [];
    public CarsTypesArray: any[] = [];
    public CompanyAdmin: any[] = [];

    constructor(private http: Http, public Settings: Config) {};

    GetAllCompanies(url: string) {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            console.log("Parray", data)
            this.CompaniesArray = data
        }).toPromise();
    }

    GetAllCars(url,id)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('uid', this.Settings.UserId.toString());
        body.append('id',id);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            console.log("Cars", data)
            this.CarsArray = data
        }).toPromise();
    }

    GetCompanyById(url: string) {
        console.log("GT1");
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        console.log("GT2");
        body.append('uid', this.Settings.UserId.toString());
        console.log("GT3",window.localStorage.userid);
        body.append('id', window.localStorage.userid);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            this.CompanyAdmin = data
            console.log("GT5");
        }).toPromise();
    }

    GetCompanyBySubDomain(url: string, sub:string)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('sub', sub);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {

        }).toPromise();
    }
    
    DelImageCar(url: string, carid , i)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('carId', carid);
        body.append('imageId', i);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {}).toPromise();
    }
    
    GetCarTypes(url: string)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {this.CarsTypesArray = data;}).toPromise();
    }

    GetModels(url: string , manufacturer)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('manufacturer', manufacturer);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {}).toPromise();
    }

    SendSms(url: string , message , phone)
    {
        let smsUrl = 'http://tapper.co.il/send_sms.php'
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('message', message);
        body.append('phone', phone);
        return this.http.post(smsUrl, body).map(res => res).do((data) => {}).toPromise();
    }

    GetCarsUpTo30(url: string)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {}).toPromise();
    }

    DelModel(url: string , id ,carId)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('id', id);
        body.append('carId', carId);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {}).toPromise();
    }

    AddModel(url: string , carId ,data)
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('data', data);
        body.append('carId', carId);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {}).toPromise();
    }

    GetCarById(url: string , carId )
    {
        let body = new FormData();
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({headers: headers});
        body.append('carId', carId);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {}).toPromise();
    }
};


