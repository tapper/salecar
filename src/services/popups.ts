
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {AlertController} from "ionic-angular";


@Injectable()

export class PopupService
{

    constructor(public alertCtrl:AlertController) { };

    presentAlert(Title , Message,PageRedirect, fun:any = null) {
        let alert = this.alertCtrl.create({
            title: Title,
            subTitle: Message,
            buttons: [{
                text: 'סגור',
                handler: () => {if(PageRedirect == 1) {fun; }}
            }],
            cssClass: 'alertRtl'
        });
        alert.present();
    }
}


