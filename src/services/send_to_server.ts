import {Http, Headers, Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import {HttpModule} from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";


const ServerUrl = "http://tapper.co.il/salecar/laravel/public/api/";

@Injectable()

export class SendToServerService {
    public ProductsArray;
    public AboutArray;
    public newsletter;
    public MainCategories;

    constructor(private http: Http, public Settings: Config) {
    };

    GetAllProducts(url: string) {
        let body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            console.log("Parray", data), this.ProductsArray = data
        }).toPromise();
    }

    GetAbout(url) {
        let body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            this.AboutArray = data
        }).toPromise();
    }


    GetCategories(url) {
        let body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            this.MainCategories = data;
            //console.log("Categories : " , data)
        }).toPromise();
    }

    SendBasketToServer(url, Basket, sum, pay) {
        var userid = window.localStorage.userid;
        let body = 'userid=' + userid.toString() + '&basket=' + JSON.stringify(Basket) + '&sum=' + sum.toString() + '&pay=' + pay.toString();
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res).do((data) => {
            console.log("Baskett : ", data)
        }).toPromise();

    }

    sendContactDetails(url) {
        let body = 'name=' + this.Settings.ContactDetails['name'] + '&details=' + this.Settings.ContactDetails['details'] + '&mail=' + this.Settings.ContactDetails['mail'] + '&phone=' + this.Settings.ContactDetails['phone'];
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res).do((data) => {
            console.log("Contact : ", data)
        }).toPromise();
    }

    LoginUser(url, params) {

        let body = 'mail=' + params.mail + '&password=' + params.password;
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Login : ", data)
        }).toPromise();
    }

    RegisterUser(url, params) {

        if (params.newsletter == true)
            this.newsletter = 1;
        else
            this.newsletter = 0;

        let body = 'name=' + params.name + '&mail=' + params.mail + '&password=' + params.password + '&address=' + params.address + '&phone=' + params.phone + '&newsletter=' + this.newsletter;

        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Register: : ", data)
        }).toPromise();
    }

    ForgotPassword(url, params) {

        let body = 'mail=' + params.mail;
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Forgot : ", data)
        }).toPromise();
    }

    UpdateCar(url, params) {

        let body = 'company_id='+params.company_id+'&color=' + params.color + '&description=' + params.description + '&hand=' + params.hand + '&id=' + params.id + '&km=' + params.km + '&manufacturer=' + params.manufacturer+ '&model=' + params.model + '&price=' + params.price + '&size=' + params.size + '&year=' + params.year;

        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Register: : ", data)
        }).toPromise();
    }
    
    DelCar(url, params) {
        
        let body = 'id=' + params.id;
        
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        
        let options = new RequestOptions({
            headers: headers
        });
        
        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Register: : ", data)
        }).toPromise();
    }
    
    AddCar(url, params) {
        
        let body = 'company_id='+params.company_id+'&color=' + params.color + '&description=' + params.description + '&hand=' + params.hand  + '&km=' + params.km + '&manufacturer=' + params.manufacturer+ '&model=' + params.model + '&price=' + params.price + '&size=' + params.size + '&year=' + params.year;
        
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        
        let options = new RequestOptions({
            headers: headers
        });
        
        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Register: : ", data)
        }).toPromise();
    }

    UpdateCompany(url, params) {

        let body = 'id='+params.id+'&name=' + params.name + '&description=' + params.description + '&address=' + params.address + '&phone=' + params.phone + '&email=' + params.email + '&subdomain=' + params.subdomain+ '&password=' + params.password;

        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Register: : ", data)
        }).toPromise();
    }


    AddCompany(url, params) {

        let body = 'name=' + params.name + '&description=' + params.description + '&address=' + params.address + '&phone=' + params.phone + '&email=' + params.email + '&subdomain=' + params.subdomain+ '&password=' + params.password;

        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("Register: : ", data)
        }).toPromise();
    }

    
};


