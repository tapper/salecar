import { Component } from '@angular/core';
import {SocialSharing} from "@ionic-native/social-sharing";
import {InAppBrowser} from "@ionic-native/in-app-browser";

/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'footer',
  templateUrl: 'footer.html'
})
export class FooterComponent {

  text:string;
  browser;


    constructor(private socialSharing: SocialSharing , private iab: InAppBrowser) {

  }

  Call()
  {
      window.location.href = 'tel:+97225654543';
  }

  Facebook()
  {
        //window.location.href = 'https://www.facebook.com/dagimlabait/?fref=ts';
      this.browser = this.iab.create('https://www.facebook.com/dagimlabait/?fref=ts', '_blank', 'location=yes');
      //browser.executeScript(...);
      //browser.insertCSS(...);
     // browser.close();
  }

  Mail()
  {
      window.location.href = 'mailto:Dan2772@gmail.com';
  }

  Waze()
  {
        window.location.href = "waze://?q=31.766114,35.215068&navigate=yes";
  }

  WhatsApp()
  {
// Share via WhatsApp
      this.socialSharing.shareViaWhatsApp('גם אני הזמנתי דגים באפליקציית דגים עד הבית להורדה לחצו כאן', '', 'linktoandroidoriphone').then(() => {
          // Success!
      }).catch(() => {
          // Error!
      });
  }
}
