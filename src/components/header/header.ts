import {Component, NgZone, OnInit} from '@angular/core';
import {SendToServerService} from "../../services/send_to_server";
import {BasketService} from "../../services/basket.service";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {BasketPage} from "../../pages/basket/basket";
import {AlertController, NavController} from "ionic-angular";
import {PopupService} from "../../services/popups";
import {HomePage} from "../../pages/home/home";

/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'header',
  templateUrl: 'header.html'
})
export class HeaderComponent implements OnInit {



  text: string;
  BasketPrice:any = this.BasketService.BasketTotalPrice;

  constructor(public navCtrl:NavController ,public BasketService:BasketService,public zone:NgZone,public alertCtrl:AlertController,public Popup:PopupService) {

      this.BasketService.BasketTotalPrice$.subscribe(val => {

          this.zone.run(() => {this.BasketPrice = val;});
      });
  }

    ionViewDidLoad() {
        console.log('ionViewDidLoad header');
    }

    goToBasket()
    {
      if (this.BasketService.basket.length > 0)
          this.navCtrl.push(BasketPage);
      else
          this.Popup.presentAlert( 'סל קניות ריק' ,'יש תחילה להזמין מוצרים','')
    }

    back()
    {
        this.Popup.presentAlert( 'סל קניות ריק' ,'יש תחילה להזמין מוצרים','')
    }

    goHome()
    {
        this.navCtrl.push(HomePage);
    }

    ngOnInit()
    {
    }



}
