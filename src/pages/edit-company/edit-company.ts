import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";
import {SendToServerService} from "../../services/send_to_server";
import {ManagerPage} from "../manager/manager";

/**
 * Generated class for the EditCompanyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-edit-company',
    templateUrl: 'edit-company.html',
})
export class EditCompanyPage {

    public Details = {
        'model': '',
        'manufacturer': '',
        'size': '',
        'year': '',
        'km': '',
        'color': '',
        'hand': '',
        'price': '',
        'description': ''
    }

    public CompanyPlace = '';
    public Company = '';

    constructor(public navCtrl: NavController, public navParams: NavParams,public Server: SendToServerService, public companyService: CompanyService) {
        this.CompanyPlace = navParams.get('CompanyId');
        this.Company = this.companyService.CompaniesArray[this.CompanyPlace];

        console.log("CP : ", this.Company)
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad EditCompanyPage');
    }

    UpdateCompany() {
        this.Server.UpdateCompany("UpdateCompany", this.Company).then(data => {
            this.navCtrl.push(ManagerPage);
        });
    }

}
