import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";
import {SubCategoriesPage} from "../sub-categories/sub-categories";

/**
 * Generated class for the CategoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {

    public CarsTypesArray: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams , public companyService: CompanyService)
  {
    this.CarsTypesArray = companyService.CarsTypesArray;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPage');
  }

    openSubCategory(i)
    {
      console.log("SendSub : " + i)
        this.navCtrl.push(SubCategoriesPage, {id: this.CarsTypesArray[i]})
    }

}
