import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {SafeResourceUrl, DomSanitizer} from '@angular/platform-browser';
import {BasketService} from "../../services/basket.service";
import {Config} from "../../services/config";

/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})

export class AboutPage implements OnInit{

  public AboutObject;
  public AboutContent;
  public AboutContentEnglish;
  public MainImg;
  public ServerUrl="http://www.tapper.co.il/dagim/php/";
  public videoUrl: SafeResourceUrl;
  public BasketPrice:any = 0;
  public defaultLangage = '';

  constructor(public navCtrl: NavController, public navParams: NavParams , public Server:SendToServerService , private domSanitizer: DomSanitizer,public BasketService:BasketService,public Settings:Config)
  {
    this.AboutObject = this.Server.AboutArray[0];
    this.AboutContent = this.AboutObject.content;
    this.AboutContentEnglish = this.AboutObject.content_english;
    this.MainImg = this.ServerUrl+''+this.AboutObject.image;
    this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.AboutObject.video)
    console.log("AboutArray : " , this.MainImg )
    this.defaultLangage = this.Settings.defaultLanguage;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
    this.CalculateBasket();

  }

  ngOnInit()
  {
    this.CalculateBasket();
  }

    CalculateBasket()
    {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice About",this.BasketPrice)
    }

}
