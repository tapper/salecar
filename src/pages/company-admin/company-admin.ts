import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";
import {AddCarPage} from "../add-car/add-car";
import {EditCarPage} from "../edit-car/edit-car";
import {LoginPage} from "../login/login";
import {SocialSharing} from "@ionic-native/social-sharing";

/**
 * Generated class for the CompanyAdminPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-company-admin',
    templateUrl: 'company-admin.html',
})
export class CompanyAdminPage {
    public Company: any[] = [];
    public Cars: any[] = [];
    public CompanyId = '';
    public CompanyPlace = '';
    // public host = "http://www.tapper.co.il/salecar/admin/public"
    public host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/"
    public message = 'הזמין אותך לצפות בכרטיס ביקור המכיל את כל הרכבים שלו לצפייה לחץ כאן :';
    public link = '';
    public fullMessage = '';
    public fullImage = '';
    public smsMessage = '';

    constructor(public navCtrl: NavController,private socialSharing: SocialSharing, public companyService: CompanyService, public navParams: NavParams, public alertCtrl: AlertController) {
        console.log("shay")
        this.companyService.GetCompanyById('GetCompanyById').then((data: any) => {
            console.log("CarAdmin : ", data), this.Company = data;
            this.Cars = this.Company['cars'];
            this.link = "http://"+this.Company['subdomain']+".salecar.co.il"
            this.fullMessage =  this.Company['name']+this.message;
            this.fullImage = this.host+"/"+this.Company['logo'];
            this.smsMessage = this.fullMessage + " " + this.link;
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CompanyAdminPage');
    }

    AddCar() {
        this.navCtrl.push(AddCarPage, {CompanyId: this.Company['id']});
    }

    EditCar(Id) {
        this.navCtrl.push(EditCarPage, {CarId: Id});
    }

    LogOut() {
        window.localStorage.userid = '';
        window.localStorage.name = '';
        this.navCtrl.push(LoginPage);
    }


    openPopUp()
    {
        let prompt = this.alertCtrl.create({
            title: 'שלח כרטיס לחבר',
            message: "על מנת לשלוח את הכרטיס ללקוח שלך הכנס מספר טלפון והוא יישלח מיידית",
            cssClass: 'alertClass',
            inputs: [
                {
                    name: 'phone',
                    placeholder: 'הכנס מספר טלפון'
                },
            ],
            buttons: [
                {
                    text: 'בטל',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'שלח',
                    handler: data => {
                        let phone = data.phone;

                        /*while(phone.charAt(0) === '0')
                        {
                            phone= "+972"+phone.substr(1);
                        }*/

                        this.companyService.SendSms('SendSms',this.smsMessage,data.phone).then((data: any) => {
                            console.log('SendSms');
                        });
                        console.log('Saved clicked : ' , phone);
                    }
                }
            ]
        });
        prompt.present();
    }

    whatsApp()
    {
        this.socialSharing.shareViaWhatsApp(this.fullMessage, this.fullImage, this.link).then(() => {
            // Success!
        }).catch(() => {
            // Error!
        });
    }

}
