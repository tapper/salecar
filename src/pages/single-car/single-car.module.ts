import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleCarPage } from './single-car';

@NgModule({
  declarations: [
    SingleCarPage,
  ],
  imports: [
    IonicPageModule.forChild(SingleCarPage),
  ],
})
export class SingleCarPageModule {}
