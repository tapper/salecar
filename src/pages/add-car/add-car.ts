import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {PopupService} from "../../services/popups";
import {ImageCarPage} from "../image-car/image-car";
import {CompanyService} from "../../services/CompanyService";

/**
 * Generated class for the AddCarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-add-car',
    templateUrl: 'add-car.html',
})
export class AddCarPage {
    
    public Details = {
        'company_id': '',
        'model': '',
        'manufacturer': '',
        'size': '',
        'year': '',
        'km': '',
        'color': '',
        'hand': '',
        'price': '',
        'description': ''
    }
    public CarsTypesArray: any[] = [];
    public CarsModelsArray: any[] = [];
    public serverResponse;
    
    constructor(public navCtrl: NavController, public navParams: NavParams,public companyService: CompanyService, public toastCtrl: ToastController, public Server: SendToServerService, public Popup: PopupService,) {
        this.Details.company_id = navParams.get('CompanyId');
        this.CarsTypesArray = this.companyService.CarsTypesArray;
        this.Details.manufacturer = this.CarsTypesArray[0].id;
        this.CarTypeChange();
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad AddCarPage');
    }
    
    
    AddCar() {

        if (this.Details.year.length < 3)
            this.presentToast('בחר שנת רכב');
        else if (this.Details.km.length < 3)
            this.presentToast('הכנס מספר קילומטר');
        else if (this.Details.color.length < 3)
            this.presentToast('בחר צבע הרכב');
        else if (this.Details.hand.length < 1)
            this.presentToast('בחר בעלים קודמים');
        else if (this.Details.price.length < 3)
            this.presentToast('הכנס מחיר');
        else {
            this.Server.AddCar("AddCar", this.Details).then(data => {
                this.navCtrl.push(ImageCarPage, {Car: data, Type: 1});
                //this.navCtrl.push(CompanyAdminPage);
            });
        }
    }
    
    public presentToast(text) {
        console.log("f3");
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    }

    CarTypeChange()
    {
        this.companyService.GetModels("GetModels", this.Details['manufacturer']).then(data => {
            this.CarsModelsArray = data;
            this.CarsTypesArray = this.companyService.CarsTypesArray;
            this.Details['model'] = this.CarsModelsArray[0].id;
            console.log("Degem : " , this.CarsModelsArray)
        });
    }
};


