import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {SendToServerService} from "../../services/send_to_server";
import {PopupService} from "../../services/popups";
import {LangChangeEvent, TranslateService} from "ng2-translate";

/**
 * Generated class for the ForgotPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {

  public Forgot =
  {
    "mail" : ""
  }
  public emailregex;
  public serverResponse;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController, public Server:SendToServerService, public Popup:PopupService, private translate: TranslateService) {

      translate.setDefaultLang('he');
      translate.onLangChange.subscribe((event: LangChangeEvent) => {

      });

  }

  sendPassword()
  {
      this.emailregex = /\S+@\S+\.\S+/;

      if (this.Forgot.mail =="")
          this.Popup.presentAlert(this.translate.instant("requiredtext"),this.translate.instant("inputmailtext"),'');

      else if (!this.emailregex.test(this.Forgot.mail)) {
          this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("requiredEmailText"),'');
          this.Forgot.mail= '';
      }
      else
      {
          this.serverResponse = this.Server.ForgotPassword("ForgotPassword",this.Forgot).then(data=>{
              console.log("forgot response: ",  data);
              if (data[0].status == 0) {
                  this.Popup.presentAlert(this.translate.instant("maildoesntexiststitletext"), this.translate.instant("maildoesntexiststext"),'');
                  this.Forgot.mail = '';
              }
              else{
                  this.Popup.presentAlert(this.translate.instant("passwordsenttitletext"), this.translate.instant("passwordsenttext"),'');
                  this.Forgot.mail= '';
                  this.navCtrl.push(LoginPage);
              }

          });
      }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPage');
  }




}
