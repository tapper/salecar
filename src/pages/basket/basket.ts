import {Component, NgZone, OnInit} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {BasketService} from "../../services/basket.service";
import {PopupPage} from "../popup/popup";
import {HomePage} from "../home/home";
import {Config} from "../../services/config";
import {RegisterPage} from "../register/register";
import {LoginPage} from "../login/login";
import {PopupService} from "../../services/popups";
import {LangChangeEvent, TranslateService} from "ng2-translate";

/**
 * Generated class for the BasketPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-basket',
  templateUrl: 'basket.html',
})
export class BasketPage implements OnInit{
    public products;
    public isAvaileble = false;
    public TotalPrice = 0;
    public ServerHost;
    public BasketPrice:any = 0;
    public Pay;
    public defaultLangage = '';
  constructor(public navCtrl: NavController,public zone:NgZone, public navParams: NavParams , public Server:SendToServerService , public BasketService:BasketService , public modalCtrl: ModalController,public Settings:Config ,private alertCtrl: AlertController, public Popup:PopupService, private translate: TranslateService) {
      this.defaultLangage = this.Settings.defaultLanguage;
      translate.onLangChange.subscribe((event: LangChangeEvent) => {

      });

      this.BasketService.BasketTotalPrice$.subscribe(val => {

          this.zone.run(() => {this.BasketPrice = val;});
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BasketPage');
  }

  ngOnInit()
  {
      this.ServerHost = this.Settings.ServerHost;
      this.products = this.BasketService.basket;
      this.TotalPrice = 0;
      for (var i = 0; i < this.products.length; i++) {
          this.TotalPrice += Number(this.products[i].Qnt) * Number(this.products[i].price);
      }

      this.CalculateBasket();
  }

    deleteProduct(index)
    {
        this.BasketService.deleteProduct(index);
    }

  SendOrderToServer()
  {
      this.Server.SendBasketToServer('getBasket', this.products ,this.TotalPrice,this.Pay).then((data: any) => {console.log("Basket : " , data) , this.showPopup()});
  }

    checkLoggedIn()
    {
        if (this.checkBasket() == true)
        {
            if (window.localStorage.userid)
            {
                if(this.Pay == '' || this.Pay == undefined)
                    this.Popup.presentAlert(this.translate.instant("selectpaytypetext"),this.translate.instant("selectpayoptiontext"),'')
                else {
                    this.SendOrderToServer();
                    this.BasketService.resetTotalPrice();
                }
            }
            else
                this.navCtrl.push(LoginPage);
        }
    }

    checkBasket()
    {
        if (this.products.length ==  0) {
            this.Popup.presentAlert(this.translate.instant("emptybaskettext"), this.translate.instant("addproductstext"),'')
            return false;
        }
        else
            return true;
    }
    CalculateBasket()
    {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice basket",this.BasketPrice)
    }

  showPopup () {
        let modal = this.modalCtrl.create(PopupPage, {url:this.translate.instant("basketpopupimage")}, {cssClass: 'FBPage'});
        modal.present();
        modal.onDidDismiss(data => {this.BasketService.emptyBasket() , this.navCtrl.setRoot(HomePage);});
  }





}
