import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";
import {SendToServerService} from "../../services/send_to_server";
import {CompanyAdminPage} from "../company-admin/company-admin";
import {ImageCarPage} from "../image-car/image-car";

/**
 * Generated class for the EditCarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-edit-car',
    templateUrl: 'edit-car.html',
})
export class EditCarPage {
    
    
    public Details = {
        'model': '',
        'manufacturer': '',
        'size': '',
        'year': '',
        'km': '',
        'color': '',
        'hand': '',
        'price': '',
        'description': ''
    }
    
    public Car;
    public CarPlace;
    public CarsTypesArray: any[] = [];
    public CarsModelsArray: any[] = [];
    
    constructor(public navCtrl: NavController, public navParams: NavParams, public companyService: CompanyService, public Server: SendToServerService) {
        this.CarPlace = navParams.get('CarId');
        this.Car = this.companyService.CompanyAdmin['cars'][this.CarPlace];
        this.Details = this.Car;
        this.CarsTypesArray = this.companyService.CarsTypesArray;
        this.CarTypeChange();
        console.log("TCar : ", this.Details)
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad EditCarPage');
    }
    
    EditCar() {
        this.Server.UpdateCar("UpdateCar", this.Details).then(data => {
            this.navCtrl.push(CompanyAdminPage);
        });
    }
    
    DelCar() {
        this.Server.DelCar("DelCar", this.Details).then(data => {
            this.navCtrl.push(CompanyAdminPage);
        });
    }
    
    goToImageCar() {
        this.navCtrl.push(ImageCarPage, {Car: this.Car,Type:0});
    }

    CarTypeChange()
    {
        this.companyService.GetModels("GetModels", this.Details['manufacturer']).then(data => {
            this.CarsModelsArray = data;
            this.CarsTypesArray = this.companyService.CarsTypesArray;
            this.Details['model'] = this.CarsModelsArray[0].id;
            console.log("Degem : " , this.CarsModelsArray)
        });
    }
}
