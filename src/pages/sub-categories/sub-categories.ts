import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";

/**
 * Generated class for the SubCategoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sub-categories',
  templateUrl: 'sub-categories.html',
})
export class SubCategoriesPage {

  public CategoryId;
    public CarsTypesArray: any[] = [];

  constructor(public navCtrl: NavController, public navParams:NavParams ,  public companyService:CompanyService ,  public alertCtrl:AlertController) {
      this.CategoryId = navParams.get('id');
      console.log("CR1 : " , this.CategoryId)
      this.companyService.GetModels("GetModels", this.CategoryId['id'] ).then(data => {
          this.CarsTypesArray = data;
          console.log("CR : " , this.CarsTypesArray)
      });
  }

  delCategory(i)
  {
      this.companyService.DelModel("DelModel", this.CarsTypesArray[i]['id'] , this.CategoryId['id'] ).then(data => {
          this.CarsTypesArray = data;
          console.log("CR : " , this.CarsTypesArray)
      });
  }

  addCategory()
  {
          let prompt = this.alertCtrl.create({
              title: 'הוסף קטגוריה',
              message: "",
              cssClass: 'alertClass',
              inputs: [
                  {
                      name: 'name',
                      placeholder: 'הכנס שם דגם'
                  },
              ],
              buttons: [
                  {
                      text: 'בטל',
                      handler: data => {
                          console.log('Cancel clicked');
                      }
                  },
                  {
                      text: 'שלח',
                      handler: data => {
                          let name = data.name;
                          this.companyService.AddModel("AddModel", this.CategoryId['id'], data.name).then(data => {
                              this.CarsTypesArray = data;
                              console.log("CR : " , this.CarsTypesArray)
                          });

                      }
                  }
              ]
          });
          prompt.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubCategoriesPage');
  }


}
