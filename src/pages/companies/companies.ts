import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";
import {SinglePage} from "../single/single";

/**
 * Generated class for the CompaniesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-companies',
    templateUrl: 'companies.html',
})
export class CompaniesPage {

    public CompaniesArray: any[] = [];
    public host = "http://www.tapper.co.il/salecar/admin/public"

    constructor(public navCtrl: NavController, public navParams: NavParams, public companyService: CompanyService) {
        this.companyService.GetAllCompanies('GetAllCompanies').then((data: any) => {
            console.log("Basket : ", data), this.CompaniesArray = data
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CompaniesPage');
    }

    goToSinglePage(Id) {
        this.navCtrl.push(SinglePage, {CompanyId: Id})
    }

}
