import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";
import {PopupPage} from "../popup/popup";

/**
 * Generated class for the CarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-car',
    templateUrl: 'car.html',
})
export class CarPage {

    public CarPlace = '';
    public Car = '';
    public CarImages: any[] = [];
    //public host = "http://www.tapper.co.il/salecar/admin/public"
    public host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/"

    constructor(public navCtrl: NavController, public companyService: CompanyService, public modalCtrl: ModalController, public navParams: NavParams) {
        this.CarPlace = navParams.get('CarPlace');
        this.Car = this.companyService.CarsArray[this.CarPlace];
        this.CarImages = this.Car['images'];
        console.log("Car ", this.Car);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CarPage');
    }

    showPopup (i) {
        let Url = this.host+''+this.CarImages[i].url;
        let modal = this.modalCtrl.create(PopupPage, {url:Url}, {cssClass: 'FBPage'});
        modal.present();
        modal.onDidDismiss(data => {});
    }

}
