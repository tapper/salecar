import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {CompanyService} from "../../services/CompanyService";
import {ManagerPage} from "../manager/manager";

/**
 * Generated class for the AddCompanyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-add-company',
    templateUrl: 'add-company.html',
})
export class AddCompanyPage {

    public Company = {
        'name': '',
        'address': '',
        'phone': '',
        'email': '',
        'subdomain': '',
        'password': '',
        'description': ''
    }


    constructor(public navCtrl: NavController, public navParams: NavParams , public Server: SendToServerService, public companyService: CompanyService) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddCompanyPage');
    }

    AddCompany() {
        this.Server.AddCompany("AddCompany", this.Company).then(data => {
            this.navCtrl.push(ManagerPage);
        });
    }

}
