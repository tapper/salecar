import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {SinglePage} from "../single/single";
import {Config} from "../../services/config";

/**
 * Generated class for the ShopPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shop',
  templateUrl: 'shop.html',
})
export class ShopPage implements OnInit{
    public Id: any;

    public products;
    public ProductsArray;
    public CategoryId;
    public SubCatId;
    public ServerHost;
    public defaultLangage = '';

  constructor(public navCtrl: NavController, public navParams: NavParams , public Server:SendToServerService,public Settings:Config) {
      this.defaultLangage = this.Settings.defaultLanguage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShopPage');
  }


  ngOnInit()
  {
      this.ServerHost = this.Settings.ServerHost;
      this.CategoryId = this.navParams.get('cat');
      this.SubCatId = this.navParams.get('subcat');
      this.products = this.Server.ProductsArray[this.CategoryId].subcat[this.SubCatId].products;
      console.log ("products:" , this.products)
  }

  goToSinglePage(ProductId)
  {
      this.navCtrl.push(SinglePage, {CategoryId: this.CategoryId,SubCatId: this.SubCatId,ProductId: ProductId})
  }
}
